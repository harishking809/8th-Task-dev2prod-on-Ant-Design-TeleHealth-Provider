import React from 'react';
import './UploadImages.scss';
import {
    PlusOutlined
} from '@ant-design/icons';

const UploadImages = () => {
    return (
        <div className="UploadImages">
            <div className="UploadImages__h1div">
                <h1>Upload Images for your Category</h1>
            </div>
            <div className="UploadImages__plus">
                <div className="UploadImages__plus__bigplus">
                    <div>
                        <PlusOutlined />
                    </div>
                </div>
                <div className="UploadImages__plus__plusicons">
                    <div className="UploadImages__plus__plusicons__first3plus">
                        <div>
                            <PlusOutlined />
                        </div>
                        <div>
                            <PlusOutlined />
                        </div>
                        <div>
                            <PlusOutlined />
                        </div>
                    </div>
                    <div className="UploadImages__plus__plusicons__second3plus">
                        <div>
                            <PlusOutlined />
                        </div>
                        <div>
                            <PlusOutlined />
                        </div>
                        <div>
                            <PlusOutlined />
                        </div>
                    </div>

                </div>
            </div>


        </div>
    )
}
export default UploadImages;