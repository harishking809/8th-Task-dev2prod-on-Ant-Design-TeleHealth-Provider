import React from 'react';
import './Description.scss';
import { Input } from 'antd';
const { TextArea } = Input;
const Description = () => {
    return (
        <div className="Description">
            <h1
                className="Description__h1"
            >Description </h1>
            <div
                className="Description__input"
            >
                <TextArea placeholder="Write few details about “Private Room”." />
            </div>
        </div>
    )
}
export default Description;