import React from 'react';
import './Add_Amenities.scss';
import {
    CloseOutlined
} from '@ant-design/icons';
import CheckBoxes from './CheckBoxes/CheckBoxes';
import { Input, Button } from 'antd';

const { Search } = Input;

const AddAmenities = () => {
    return (
        <div className="AddAmenities">
            <div className="AddAmenities__add">
                <h1
                    className="AddAmenities__add__h1">
                    Add Amenities & Facilities for Private Room </h1>
                <div
                    className="AddAmenities__add__search">
                    <div>

                        <Input
                            placeholder="Sanitizers (Paid - for guests)"

                        />
                        <Button type="primary">Add </Button>
                    </div>
                </div>
            </div>
            <div
                className="AddAmenities__suggest">
                <h1
                    className="AddAmenities__suggest__h1">
                    Suggested Amenities</h1>
                <div
                    className="AddAmenities__suggest__usersuggestions">
                    <div
                        className="AddAmenities__suggest__usersuggestions__firstrow">
                        <div> <p> Wifi <CloseOutlined /> </p> </div>
                        <div> <p> Parking <CloseOutlined /> </p> </div>
                        <div> <p> Luggage Assist.. <CloseOutlined /> </p> </div>
                        <div> <p> Pool <CloseOutlined /> </p> </div>
                        <div> <p> Washing Machine <CloseOutlined /> </p> </div>
                    </div>
                    <div
                        className="AddAmenities__suggest__usersuggestions__secondrow">
                        <div> <p> Doctor on Call <CloseOutlined /> </p> </div>
                        <div> <p> Wifi <CloseOutlined /> </p> </div>
                        <div> <p> Wifi <CloseOutlined /> </p> </div>
                        <div> <p> Wifi <CloseOutlined /> </p> </div>
                    </div>
                </div>
            </div>
            <div
                className="AddAmenities__CheckBoxes"
            >
                <CheckBoxes />
            </div>
        </div>
    )
}
export default AddAmenities;