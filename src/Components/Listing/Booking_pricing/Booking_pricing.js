import React from 'react';
import './Booking_pricing.scss';
import { Radio } from 'antd'; import { Breadcrumb } from 'antd';
import { Button } from 'antd';
import { Divider } from 'antd';

import { Input } from 'antd';
const plainOptions = ['1 month or below', '3 months or below', 'Upto 6 months'];
const Bookingpricing = () => {
    return (
        <div
            className="Bookingpricing"
        >

            <div className="Bookingpricing__bookinghtag">
                <h1> Booking & Pricing</h1>
                <h2>User can book unto how many days</h2>
            </div>
            <div className="Bookingpricing__radio">
                <div className="Bookingpricing__radio__group">
                    <Radio.Group options={plainOptions} checked />
                </div>
                <div className="Bookingpricing__radio__below">

                    <Radio
                    >  Or above mention below </Radio>
                </div>
            </div>
            <div className="Bookingpricing__divider">
                <div className="Bookingpricing__divider__outerdiv">
                    <div className="Bookingpricing__divider__innerdiv">
                        <Input />
                        <p>to </p>
                        <Input />
                    </div>
                </div>
            </div>
            <div className="Bookingpricing__Breadcrumb">
                <div className="Bookingpricing__Breadcrumb__h1">
                    <h1>Estimate the price for category room for single day</h1>
                </div>
                <div className="Bookingpricing__Breadcrumb__items">

                    <Breadcrumb >
                        <Breadcrumb.Item>$$$</Breadcrumb.Item>
                        <Breadcrumb.Item
                        >

                            <span
                                className="Bookingpricing__Breadcrumb__items__span"
                            >
                                Per day
                            </span>
                        </Breadcrumb.Item>
                    </Breadcrumb>
                </div>
            </div>
            <div
                className="Bookingpricing__payment"
            >
                <p>Check payment terms & conditions</p>
            </div>
            <div
                className="Bookingpricing__button"
            >
                <div
                    className="Bookingpricing__button__preview"
                >

                    <Button >Save & Preview</Button>
                </div>
                <div
                    className="Bookingpricing__button__exit"
                >
                    <Button >Save & Exit</Button>

                </div>
            </div>







        </div>
    )
}
export default Bookingpricing;