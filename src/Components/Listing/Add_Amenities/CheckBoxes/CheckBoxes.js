import { Checkbox } from 'antd';
import React from 'react';
import './CheckBoxes.scss';
const plainOptions1 = ['Wifi', 'Luggage Assistance', 'Housekeeping', "Housekeeping"];
const plainOptions2 = ['Tv', 'Bar', 'Refrigerator', "Refrigerator"];
const plainOptions3 = ['Parking', 'Doctor on Call', 'Washing Machine'];
const plainOptions4 = ['Gym', 'Sanitizers (Paid - for guests)', 'Dining Area'];
const plainOptions5 = ['Pool', 'In-room Safe', 'Power Backup'];


const CheckBoxes = () => {
    return (
        <div className="CheckBoxes">
            <div
                className="CheckBoxes__firstdiv"
            >
                <Checkbox.Group
                    defaultValue={['Wifi']}
                    options={plainOptions1} />
            </div>
            <div>
                <Checkbox.Group options={plainOptions2} />
            </div>
            <div>
                <Checkbox.Group options={plainOptions3} />
            </div>
            <div>
                <Checkbox.Group options={plainOptions4} />
            </div>
            <div>
                <Checkbox.Group options={plainOptions5} />
            </div>
        </div>
    )
}
export default CheckBoxes;