import React from 'react';
import { Input } from 'antd';
import './Header.scss';
import {
    BellOutlined, UserOutlined
} from '@ant-design/icons';
import { Select } from 'antd';

const { Option } = Select;
const { Search } = Input;

const Header = () => {
    return (
        <div className="Header">
            <div className="Header__imgAndSearchbar">
                <div className="Header__imgAndSearchbar__img">

                    <img src="./photos/logo.png" alt="ok" />
                </div>
                <div className="Header__imgAndSearchbar__searchbar">
                    <Search
                        placeholder="search booking/rooms" allowClear style={{
                            width: "100%",
                            "color": "black"
                        }} />
                </div>
            </div>
            <div
                className="Header__iconsAndSelect">
                <img src="./nani assests/Bell.png" alt="hah"
                    className="Header__iconsAndSelect__bell"
                />
                <img src="./nani assests/user2.png" alt="hah"
                    className="Header__iconsAndSelect__user"
                />

                <Select defaultValue="Tom Holland"
                    style={{
                        marginTop: "-.3rem",
                        "fontSize": "1.2rem"
                        , marginLeft: "-.7rem",
                        "color": "black",
                        "fontWeight": "500"

                    }}
                >
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="disabled" disabled>
                        Disabled
                    </Option>
                    <Option value="Yiminghe">yiminghe</Option>
                </Select>

            </div>
        </div>
    )
}
export default Header;