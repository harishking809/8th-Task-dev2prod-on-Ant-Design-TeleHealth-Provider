import React from 'react';
import Header from './Header/Header';
import Sidebar from './Sidebar/Sidebar';
import Listing from './Listing/Listing';
import './Components.scss';

const RootComponentsOfLisiting = () => {
    return (
        <div className="RootComponentsOfLisiting">
            <div className="RootComponentsOfLisiting__Header">
                <Header />
            </div>
            <div className="RootComponentsOfLisiting__sidebarPlusListing">
                <div className="RootComponentsOfLisiting__sidebarPlusListing__Sidebar">
                    <Sidebar />
                </div>
                {/* <div className="RootComponentsOfLisiting__sidebarPlusListing__positionProblem">
                </div> */}
                <div className="RootComponentsOfLisiting__sidebarPlusListing__Listing">
                    <h1
                        className="RootComponentsOfLisiting__sidebarPlusListing__Listing__h1"
                    >Listings</h1>
                    <Listing />
                </div>
            </div>

        </div>
    )
}
export default RootComponentsOfLisiting;