import React from 'react';
import { AccountBookFilled, PauseCircleFilled, RedEnvelopeFilled, MoneyCollectFilled, ZhihuSquareFilled, ToolFilled, ThunderboltFilled } from '@ant-design/icons';
import './Sidebar.scss';

const Sidebar = () => {
    return (
        <div className="Sidebar">
            <div
                className="Sidebar__dashbord"
            >
                <img
                    className="sidebar__img1"
                    src="./nani assests/2.png" alt="ok" />
                <p
                    className="Sidebar__activep"
                >Dashboard</p>
            </div>

            <div
                className="Sidebar__Calender"
            >
                <img
                    className="sidebar__img1"
                    src="./nani assests/calendar4.png" alt="ok" />

                <p>Calender</p>
            </div>

            <div
                className="Sidebar__Listings"
            >

                <img
                    className="sidebar__img1"
                    src="./nani assests/listings2.png" alt="ok" />
                <p>Listings</p>
            </div>

            <div
                className="Sidebar__payments"
            >

                <img
                    className="sidebar__img1"
                    src="./nani assests/payments2.png" alt="ok" />
                <p>Payments</p>
            </div>

            <div
                className="Sidebar__Reports"
            >

                <img
                    className="sidebar__img1"
                    src="./nani assests/statistics2.png" alt="ok" />
                <p> Reports </p>
            </div>

            <div
                className="Sidebar__User"
            >



                <img
                    className="sidebar__img1"
                    src="./nani assests/group2.png" alt="ok" />
                <p>User Management </p>
            </div>
            <div

                className="Sidebar__logout"
            >

                <img
                    className="sidebar__img1"
                    src="./nani assests/logout.png" alt="ok" />
                <p>
                    Log out </p>
            </div>
        </div>
    )
}
export default Sidebar;