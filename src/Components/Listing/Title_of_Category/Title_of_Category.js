import React from 'react';
import './Title_of_Category.scss';
import { Breadcrumb } from 'antd';
import { Input } from 'antd';
import {
    PlusOutlined, MinusOutlined
} from '@ant-design/icons';
const TitleofCategory = () => {
    return (
        <div className="TitleofCategory">
            <div className="TitleofCategory__Breadcrumb">
                <Breadcrumb separator=">">
                    <Breadcrumb.Item>Category List</Breadcrumb.Item>
                    <Breadcrumb.Item href="">Category Creation</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <div className="TitleofCategory__Title">
                <h1>Title of Category</h1>
            </div>
            <div className="TitleofCategory__input">
                <Input placeholder="“Type Category name”" />
            </div>
            <div className="TitleofCategory__Createroom">
                <h1 className="TitleofCategory__Createroom__h1">
                    Create Room</h1>
                <div className="TitleofCategory__Createroom__miuszeroplus">

                    <div className="TitleofCategory__Createroom__mimus">

                        <MinusOutlined />
                    </div>
                    <div className="TitleofCategory__Createroom__zero">
                        <h1> 0</h1>
                    </div>

                    <div className="TitleofCategory__Createroom__plus">

                        <PlusOutlined />
                    </div>

                </div>

            </div>
        </div>
    )
}
export default TitleofCategory;