import React from 'react';
import TitleofCategory from './Title_of_Category/Title_of_Category';
import './Listing.scss';
import UploadImages from './UploadImages/UploadImages';
import AddAmenities from './Add_Amenities/Add_Amenities';
import Description from './Description/Description';
import Bookingpricing from './Booking_pricing/Booking_pricing';

const Listing = () => {
    return (
        <div className="Listing">
            <TitleofCategory />
            <UploadImages />
            <Description />
            <AddAmenities />
            <Bookingpricing />
        </div>
    )
}
export default Listing;